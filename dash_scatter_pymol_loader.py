#!/usr/bin/env python
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2020-01-20 09:33:11 (UTC+0100)


import sys
sys.path.append('/home/bougui/source/pymol-open-source/build_python2/lib/python')
import pymol
from pymol import cmd
from data_processing import utils
from data_processing import Density
import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import os
import numpy as np
import pandas as pd
sys.path.append('/home/bougui/source/dashplot/')
import dashplot

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
df = pd.read_csv('evaluation_apo.csv')
plots = []
plots.append(html.H1(children='Pymol loader plot'))
plots.append(html.Div(id='loaded_data'))
pymol_scatter = dashplot.plot_scatter(df, ['SASA', ],
                                      ['pmax', ],
                                      'pymol_scatter',
                                      labels=['id', ])
plots.append(pymol_scatter)
app.layout = html.Div(plots)


def get_pmax_pockets(grid, labels):
    pmax_list = []
    for label in np.unique(labels):
        pmax_list.append(np.max(grid[labels == label]))
    return pmax_list


@app.callback(Output('loaded_data', 'children'), [Input('pymol_scatter',
                                                        'clickData')])
def load_pymol(clickData):
    ind = int(clickData['points'][0]['text'])
    pocket_id = int(df.loc[[ind]]['rank_max_importance'].item())
    pdbapo = df.loc[[ind]]['pdb1'].item()
    npzfile = df.loc[[ind]]['npz'].item()
    pdbpartner = df.loc[[ind]]['pdb2'].item()
    pdbholo = df.loc[[ind]]['pdb3'].item()
    pmean = float(df.loc[[ind]]['pmean'].item())
    rank = float(df.loc[[ind]]['rank_max_importance'].item())
    cmd.delete('all')
    cmd.load(pdbpartner, 'partner')
    cmd.load(pdbapo, 'apo')
    cmd.load(pdbholo, 'holo')
    cmd.color('gray80', 'apo')
    cmd.color('orange', 'holo')
    cmd.color('cyan', 'partner')
    cmd.remove('solvent')
    npzfile = np.load(npzfile)
    grid = np.squeeze(npzfile['hd'])
    origin = npzfile['origin']
    grid = grid[-1]
    grid = 1. - grid
    print(grid.shape)
    labels = utils.watershed(grid)
    print(get_pmax_pockets(grid, labels))
    grid[labels != pocket_id] = 0.
    utils.save_density(grid, 'tmp.mrc', 1, origin, 6)
    cmd.load('tmp.mrc', 'hdpred')
    cmd.isosurface('hdsurf', 'hdpred', level=pmean)
    cmd.color('red', 'hdsurf')
    cmd.set('transparency', .375)
    return '%s; holo: %s; partner: %s; rank: %d; level: %.2f' % (pdbapo,
                                                                 pdbholo,
                                                                 pdbpartner,
                                                                 rank,
                                                                 pmean)

pymol.finish_launching()
app.run_server(debug=False)
