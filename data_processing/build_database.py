import os
import sys

import collections
from distutils.dir_util import copy_tree
import glob
import h5py
import numpy
import pymol.cmd as cmd
from shutil import copyfile
from sklearn.cluster import KMeans
from tqdm import tqdm

sys.path.append('../')

if __name__ == '__main__':
    script_dir = os.path.dirname(os.path.realpath(__file__))
    sys.path.append(os.path.join(script_dir, '..'))

from data_processing import Density, utils, Complex

"""
We start with two databases containing structures of proteins and their partners.
The HD database contains two PDB files named {pdb}-{chain1chain2}-{uniprot1}-{uniprot2}-{chain_at_hands}.pdb, where
chain at hands can be either chain1 or chain 2.
The PL database contains PDB files named {pdb}-{chain}-{uniprot}.pdb, and pdb files for ligands
We also have an apo database that is flat and contains files like {uniprot}_{apo_pdb}_{holo_pdb}.pdb

For faster access, the files are organized in a hierachical decomposition based on the holo pdb code as such :
1emv pdbs are stored in 1/e/m/v/

Finally, we have a text format of this database that lists all of the systems 
and also train/val/test splits based on CATH classes.

The goal is to merge those databases and include the relevant apo files in the relevant directories.
These apo files are called {uniprot}_{chain_name_in_holo}_{pdb_apo}_{pdb_holo}.pdb
"""


def parse_chunks(chunked_txtfile, clean_txtfile):
    """
    The train and test files are chunked for some reason.
    :param chunked_txtfile: The path of the file
    :return: the list of original files
    """

    outlines = set()
    with open(chunked_txtfile, 'r') as dbfile:
        lines = dbfile.readlines()
        for line in tqdm(lines):
            line = line.strip()
            P, L = line.split(" ")
            pdbl, chains, uni1, uni2, chain, *_ = L.split('-')
            pruned = f"{P} {pdbl}-{chains}-{uni1}-{uni2}-{chain}.pdb\n"
            outlines.add(pruned)
    with open(clean_txtfile, 'w') as wholefile:
        wholefile.writelines(sorted(outlines))


def copy_db(database_hd, database_pl, database_dst):
    """
    Merge the PL and HD data sets into one tree
    :param database_hd:
    :param database_pl:
    :param database_dst:
    :return:
    """
    copy_tree(src=database_hd, dst=database_dst)
    copy_tree(src=database_pl, dst=database_dst)


def get_apos(list_of_holos, holo_database_path, apo_database_path, apolistdir_path=None, hd=True, overwrite=False):
    """
    Given a list of holos at holo_database_path, save corresponding apos found in apo_database_path in the right dir.

    :param list_of_holos:
    :param holo_database_path:
    :param apo_database_path:
    :param apolistdir_path:
    :return:
    """
    with open(list_of_holos, 'r') as f:
        holos = f.readlines()
    holos = [holo.strip() for holo in holos]

    # Use precomputed listdir as it is costly on the serv
    if apolistdir_path is not None:
        with open(apolistdir_path, 'r') as f:
            apolistdir = f.readlines()
        apolistdir = [apo.strip() for apo in apolistdir]
    else:
        apolistdir = os.listdir(apo_database_path)

    # Now build a mapping holo_pdb:list_of_apo_filenames
    apo_hash = collections.defaultdict(list)
    for apo in apolistdir:
        corresponding_holo_pdb = apo.split('_')[2].split('.')[0]
        apo_hash[corresponding_holo_pdb].append(apo)

    for holo in tqdm(holos):
        # P:TO_KEEP.pdb L:*  -> TO_KEEP
        receptor_name = holo.split(' ')[0][2:-4]
        if hd:
            # TO_KEEP =  {pdb}-{chain1chain2}-{uniprot1}-{uniprot2}-{chain_at_hands}
            holo_pdb, chain1chain2, uniprot1, uniprot2, chain_at_hands = receptor_name.split('-')
            index_chain = chain1chain2.find(chain_at_hands)
            uniprot_at_hands = [uniprot1, uniprot2][index_chain]
        else:
            # TO_KEEP : '{pdb}-{chain}-{uniprot}
            holo_pdb, chain_at_hands, uniprot_at_hands, = receptor_name.split('-')

        for apo_filename in apo_hash[holo_pdb]:
            uniprot_apo, pdb_apo, pdb_holo = apo_filename[:-4].split('_')
            if uniprot_apo == uniprot_at_hands:
                dump_name = f"apo_{uniprot_at_hands}_{chain_at_hands}_{pdb_apo}.pdb"
                dump_path_indb = pdbcode_to_path(holo_pdb, dump_name)
                dump_path = os.path.join(holo_database_path, dump_path_indb)
                src_path = os.path.join(apo_database_path, apo_filename)
                # print(src_path)
                # print(dump_path)
                if overwrite or not os.path.exists(dump_path):
                    copyfile(src_path, dump_path)


def get_shorts(txtfile, database_path):
    """
    For all systems in txtfile, split the ligand to get the peptide
    :param txtfile:
    :return:
    """
    with open(txtfile, 'r') as dbfile:
        lines = dbfile.readlines()
        for line in tqdm(lines):
            line = line.strip()
            receptor_pdbfilename = line.split(" ")[0][2:]
            ligand_pdbfilename = line.split(" ")[1][2:]
            dump_name = ligand_pdbfilename[:-4] + '-short.pdb'

            pdbcode = receptor_pdbfilename[:4]
            receptor_path = utils.pdbcode_to_path(pdbcode, receptor_pdbfilename, prepend_dir=database_path)
            ligand_path = utils.pdbcode_to_path(pdbcode, ligand_pdbfilename, prepend_dir=database_path)
            dump_path = utils.pdbcode_to_path(pdbcode, dump_name, prepend_dir=database_path)
            cmd.load(receptor_path, 'P')
            cmd.load(ligand_path, 'L')
            cmd.remove('hydrogens')
            cmd.remove('L and not (byres P around 6.)')
            cmd.save(dump_path, 'L')
            cmd.delete('all')


def get_grid_size(ligand, padding, spacing):
    """
    Get the size of the ligand grid.
    We will then use it to decide whether the short.pdb should be split into short_{1,2..}

    :param ligand:
    :param padding:
    :param spacing:
    :return:
    """
    cmd.delete('all')
    cmd.load(ligand, 'ligand')
    cmd.remove('hydrogens')
    coords = cmd.get_coords(selection='ligand')
    cmd.delete('all')
    nx, ny, nz = Complex.get_grid_shape(coords.min(axis=0), coords.max(axis=0), spacing, padding)
    return nx, ny, nz


def cluster(pdbfilename, n_clusters):
    """
    Take a short.pdb and cut it into n_clusters pieces
    >>> cluster('../data/2lj5.pdb', 4)
    >>>

    :param pdbfilename:
    :param n_clusters:
    :return:
    """
    # First cluster the coordinates
    cluster = KMeans(n_clusters=n_clusters)
    cmd.load(pdbfilename, 'peptide')
    cmd.remove('hydrogens')
    coords, topology = Density.get_topology('peptide', 'all')
    labels = cluster.fit_predict(coords)
    clusters = []

    # Now for each label, we get the residues for which an atom is in this cluster label
    for label in numpy.unique(labels):
        resids = numpy.unique(topology['resids'][labels == label])
        selection = 'resi '
        for resid in resids:
            selection += f"+{resid}"
        outpdbfilename = os.path.splitext(pdbfilename)[0] + '_%d.pdb' % label
        cmd.save(outpdbfilename, selection)
        clusters.append(cmd.get_coords(selection))
    return clusters


def get_chunks(txtfile, database_path, size_threshold=30000, padding=0, spacing=1):
    """
    For all systems in txtfile, split the ligand to get the peptide
    :param txtfile:
    :return:
    """
    with open(txtfile, 'r') as dbfile:
        lines = dbfile.readlines()
        for line in tqdm(lines):
            line = line.strip()
            receptor_pdbfilename = line.split(" ")[0][2:]
            ligand_pdbfilename = line.split(" ")[1][2:]
            pdbcode = receptor_pdbfilename[:4]
            short_name = ligand_pdbfilename[:-4] + '-short.pdb'
            short_path = utils.pdbcode_to_path(pdbcode, short_name, prepend_dir=database_path)
            nx, ny, nz = get_grid_size(short_path, padding=padding, spacing=spacing)
            grid_size = nx * ny * nz
            n_clusts = grid_size // size_threshold + 1
            if n_clusts > 1:
                cluster(short_path, n_clusters=n_clusts)


def add_to_h5(database_txt, database_path, h5filename, hetatm):
    """

    :param database_txt: name of the text file of the DB
    :param database_path: path to the database in PDB format
    :param h5filename: name of the hdf5 file
    :param hetatm: If a ligand
    :return: None
    """
    with h5py.File(h5filename, 'a') as h5file:
        with open(database_txt) as pldb:
            protlig = utils.get_protlig_list(pldb, database_path)
            n = len(protlig)
            for i, (prot, lig) in enumerate(protlig):
                sys.stdout.write('%.2f %%: %s-%s' % ((i + 1) * 100. / n, prot, lig))
                # try:
                density = Density.Coords_channel(prot, lig, hetatm=hetatm)
                density.add_to_h5(h5file=h5file)
                density.add_to_h5(h5file=h5file, key='lig')
                sys.stdout.write(utils.bcolors.OKGREEN + ' [OK]' + utils.bcolors.ENDC + '\n')
                # except:
                #    sys.stdout.write(utils.bcolors.FAIL + ' [FAILED]' + utils.bcolors.ENDC + '\n')


def build_hdf5_fromtxt(database_txt, database_path, h5filename, hd=True):
    """
    A function that fills an hdf5 up with all systems of a db.

    :param db_path:
    :return:
    """
    with h5py.File(h5filename, 'a') as h5file:
        with open(database_txt, 'r') as db_txt:
            lines = db_txt.readlines()
            for line in tqdm(lines):
                line = line.strip()
                receptor_pdbfilename = line.split(" ")[0][2:]
                receptor_name = receptor_pdbfilename[:-4]
                ligand_pdbfilename = line.split(" ")[1][2:]
                pdbcode = receptor_pdbfilename[:4]
                dir_path = os.path.join(database_path, utils.pdbcode_to_path(pdbcode=pdbcode))
                receptor_path = os.path.join(dir_path, receptor_pdbfilename)
                ligand_path = os.path.join(dir_path, ligand_pdbfilename)

                if hd:
                    # TO_KEEP =  {pdb}-{chain1chain2}-{uniprot1}-{uniprot2}-{chain_at_hands}
                    holo_pdb, chain1chain2, uniprot1, uniprot2, chain_at_hands = receptor_name.split('-')
                    index_chain = chain1chain2.find(chain_at_hands)
                    uniprot_at_hands = [uniprot1, uniprot2][index_chain]
                    chain_other = chain1chain2[(index_chain + 1) % 2]
                    ligands = glob.glob(os.path.join(dir_path, f'*{chain_other}-short*'))
                    if len(ligands) > 1:
                        ligands = [lig for lig in ligands if not lig.endswith('-short.pdb')]
                    for lig in ligands:
                        density = Density.Coords_channel(receptor_path, lig, hetatm=False)
                        density.add_to_h5(h5file=h5file)
                        density.add_to_h5(h5file=h5file, key='lig')

                else:
                    # TO_KEEP : '{pdb}-{chain}-{uniprot}
                    holo_pdb, chain_at_hands, uniprot_at_hands, = receptor_name.split('-')
                    density = Density.Coords_channel(receptor_path, ligand_path, hetatm=True)
                    density.add_to_h5(h5file=h5file)
                    density.add_to_h5(h5file=h5file, key='lig')

                apos = {file for file in os.listdir(dir_path) if file.startswith(f'apo_{uniprot_at_hands}')}
                for apo in apos:
                    apo_path = os.path.join(dir_path, apo)
                    density = Density.Coords_channel(apo_path, hetatm=True)
                    density.add_to_h5(h5file=h5file, key='apo', pdbfilename_p=receptor_path)


def fix_hdf5(h5filename):
    """
    Just to move the apo and ligs in clean groups.

    :param hdf5:
    :return:
    """
    with h5py.File(h5filename, 'a') as h5file:
        proteins = list(h5file.keys())
        for prot in proteins:
            prot_dir = h5file[prot]
            print(prot, set(prot_dir.keys()))
            liglist = set(prot_dir.keys()) - {'coords'} - {'ligs'} - {'apos'}
            if 'ligs' not in prot_dir:
                groupligs = prot_dir.create_group('ligs')
            else:
                groupligs = prot_dir['ligs']
            if 'apos' not in prot_dir:
                groupapos = prot_dir.create_group('apos')
            else:
                groupapos = prot_dir['apos']
            apos = {lig for lig in liglist if lig.startswith('apo_')}
            ligs = liglist - apos
            for lig in ligs:
                src = os.path.join(prot, lig)
                dest = os.path.join(prot, 'ligs', lig)
                h5file.move(src, dest)
            for apo in apos:
                src = os.path.join(prot, apo)
                dest = os.path.join(prot, 'apos', apo)
                h5file.move(src, dest)


if __name__ == '__main__':
    pass

    database_hd = '/c7/scratch/kdruart/ippidb_update_strict/HD-database'  # Original location of the files organized flat
    database_pl = '/c7/scratch/kdruart/ippidb_update_strict/PL-database'
    hd_db_txt = '../data/HD-database.txt'  # List of HD systems
    pl_db_txt = '../data/PL-database.txt'  # List of PL systems
    database_path = '../data/hdpl'  # Path to the directory where to gather all files
    h5filename = '../data/hdpl-database.hdf5'  # Path where to dump the hdf5 file

    # First copy the files from several remote sources.
    # Get the splits lists in the txt format
    # parse_chunks(chunked_txtfile='../data/HD-database_chunks_training_set.txt',
    #              clean_txtfile='../data/HD-database_training_set.txt')
    # copy_db(database_hd=database_hd,
    #         database_pl=database_pl,
    #         database_dst=database_path)

    # Then complete this database by adding the structure of apo for each uniprot present in the holo list
    get_apos(list_of_holos=hd_db_txt,
             holo_database_path=database_path,
             apo_database_path='/c7/scratch/kdruart/apo_dataset/HD_align',
             apolistdir_path='../data/apo_hd_list.txt')
    get_apos(list_of_holos=pl_db_txt,
             holo_database_path=database_path,
             apo_database_path='/c7/scratch/kdruart/apo_dataset/PL_align',
             apolistdir_path='../data/apo_pl_list.txt',
             hd=False)

    # Then cut the HD partners into small pieces close to the surface and into fixed size chunks
    get_shorts(txtfile='../data/HD-database.txt', database_path=database_path)
    get_chunks(txtfile='../data/HD-database.txt', database_path=database_path)

    # Finally organize all those files (receptor holo and apo, partner chunked if HD) in a hdf5 file
    build_hdf5_fromtxt(hd_db_txt, database_path, h5filename, hd=True)
    build_hdf5_fromtxt(pl_db_txt, database_path, h5filename, hd=False)

    # fix_hdf5(h5filename=h5filename)
