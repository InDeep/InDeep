#!/usr/bin/env python3
# -*- coding: UTF8 -*-

# Author: Guillaume Bouvier -- guillaume.bouvier@pasteur.fr
# https://research.pasteur.fr/en/member/guillaume-bouvier/
# 2019-09-12 09:38:26 (UTC+0200)

import os
import pymol.cmd as cmd
import numpy

import sys

script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_dir, '..'))

from data_processing import utils


def get_topology(object_name, selection):
    """
    • filename: pdb file name
    """
    selection = "%s and %s" % (object_name, selection)
    coords = cmd.get_coords(selection=selection)
    topology = {'resnames': [],
                'names': [],
                'chains': [],
                'resids': [],
                'vdws': []}
    cmd.iterate(selection,
                'resnames.append(resn);names.append(name);chains.append(chain);resids.append(resi);vdws.append(vdw)',
                space=topology)
    for k in topology:
        topology[k] = numpy.asarray(topology[k])
        # if k == 'resids': # Removed to handle residue insertion
        #    topology[k] = numpy.int_(topology[k])
    topology['counts'] = numpy.arange(len(topology['resids']))
    # cmd.delete(object_name)
    return coords, topology


def get_selection(atomtype, atomtype_mapping):
    sel_list = atomtype_mapping[atomtype]
    selection = ''
    for i, ra in enumerate(sel_list):
        resname, atomname = ra
        if i > 0 and i < len(sel_list):
            selection += ' | '
        selection += '(resn %s & name %s)' % (resname, atomname)
    return selection


ATOMTYPES = utils.read_atomtypes()
ATOMTYPES = {atomtype: get_selection(atomtype, ATOMTYPES) for atomtype in ATOMTYPES}


class Coords_channel(object):
    def __init__(self, pdbfilename_p, pdbfilename_l=None,
                 hetatm=False):
        """
        The goal is to go from pdb files and optionnally some selections to the (n,4) or (n,1) format of coordinates
        annotated by their channel id (or 1 for hetatm). This can be used to fill an hdf5 if it is not None

        - pdbfilename_p: PDB file name for the protein
        - pdbfilename_l: PDB file name for the ligand
        - hetatm: Boolean. Set to True for HETATM ligand
        - h5file: HDF5 file object from h5py.File
        """
        a = cmd.get_object_list('all')
        safe_sel = ['None', 'None'] + list({'prot', 'lig'}.intersection(set(a)))
        safe_sel = ' or '.join(safe_sel)
        cmd.delete(safe_sel)
        self.pdbfilename_p = pdbfilename_p
        self.pdbfilename_l = pdbfilename_l
        self.hetatm = hetatm
        # self.atomtypes = utils.read_atomtypes()
        self.atomtypes = ATOMTYPES
        cmd.load(pdbfilename_p, 'prot')
        if pdbfilename_l is not None:
            cmd.load(pdbfilename_l, 'lig')
            self.ligand_selection = 'lig'
        else:
            self.ligand_selection = None
        cmd.remove('hydrogens')
        self.prot_selection = 'prot'

        a = cmd.get_object_list('all')
        safe_sel = ['None', 'None'] + list({'prot', 'lig'}.intersection(set(a)))
        safe_sel = ' or '.join(safe_sel)

        _, self.topology = get_topology('prot', safe_sel)
        self.coords = None
        # self.set_coords()

    def set_coords(self, coords=None, selection='prot or lig'):
        if coords is not None:
            cmd.load_coords(coords, selection)
        self.coords = cmd.get_coords(selection='%s or %s' % (self.prot_selection,
                                                             self.ligand_selection))
        self.coords_prot = cmd.get_coords(selection=self.prot_selection)
        if self.ligand_selection is not None:
            self.coords_lig = cmd.get_coords(selection=self.ligand_selection)
        else:
            self.coords_lig = None

        self.coords_channels_prot = self.split_coords_by_channels()
        self.coords_channels_lig = self.split_coords_by_channels(ligand=True,
                                                                 hetatm=self.hetatm)

    def split_coords_by_channels(self, selection=None, hetatm=False, ligand=False, load_coords=None):
        """
        Build the grid.
        - selection: pymol selection string to compute the grid on
        - ligand: input or output
        - hetatm: if True consider the ligand as hetero-atoms and
                  do not decompose in channels
        """
        initial_selection = self.ligand_selection if ligand else self.prot_selection
        if selection is not None:
            selection = initial_selection + ' and ' + selection
        else:
            selection = initial_selection

        if load_coords is not None:
            cmd.load_coords(load_coords, selection)
            # raw_coords = cmd.get_coords(selection=selection)
            # print('raw coords : ', raw_coords.shape)

        coords_all = None
        atomtypes = {'ALL': 'all'} if hetatm else self.atomtypes
        for cid, atomtype in enumerate(atomtypes):  # cid: channel id
            if hetatm:
                cid = -1
                coords = cmd.get_coords(selection=selection)
            else:  # Channels
                coords = cmd.get_coords(selection='%s and (%s)' % (selection,
                                                                   atomtypes[atomtype]))
            if coords is not None:
                if coords_all is None:
                    # Store the coordinates in a (n, 4) array (coords_all)
                    # with the last column corresponding to the channel id
                    coords_all = numpy.c_[coords,
                                          numpy.repeat(cid, coords.shape[0])]
                else:
                    coords_all = numpy.r_[coords_all,
                                          numpy.c_[coords,
                                                   numpy.repeat(cid, coords.shape[0])]]
        return coords_all

    def add_to_h5(self, h5file, coords=None, key='prot', pdbfilename_p=None, overwrite=False):
        """
        Take either the protein receptor, the ligand or an apo form and put them in the protein receptor group.

        The name of the group is as follows :
        holo_pdb/
            coords
            ligs/
                lig1/coords
                lig2/coords
            apos/
                apo1/coords

        :param h5file:
        :param coords:
        :param key:
        :param pdbfilename_p:
        :return:
        """
        # A little tricky to use the receptor pdb by default, but to specify the holo one when using apo complexes.
        if pdbfilename_p is None:
            pdbfilename_p = self.pdbfilename_p
        groupname = os.path.basename(pdbfilename_p)
        if groupname not in h5file:
            group = h5file.create_group(groupname)
        else:
            group = h5file[groupname]

        # Now that we have our group name, populate it.
        if key == 'prot':
            if not overwrite and 'coords' in group.keys():
                return
            if coords is None:
                coords = self.split_coords_by_channels()
            group.create_dataset('coords', data=coords)
            return

        # create group for the ligands
        elif key == 'lig':
            if 'ligs' not in group.keys():
                groupligs = group.create_group('ligs')
            else:
                groupligs = group['ligs']

            ligname = os.path.basename(self.pdbfilename_l)
            if not overwrite and ligname in groupligs.keys():
                return
            if coords is None:
                coords = self.split_coords_by_channels(ligand=True, hetatm=self.hetatm)
            grouplig = groupligs.create_group(ligname)
            grouplig.create_dataset('coords', data=coords)
            return

        # create group for the apos
        elif key == 'apo':
            if 'ligs' not in group.keys():
                groupapos = group.create_group('apos')
            else:
                groupapos = group['apos']
            aponame = os.path.basename(self.pdbfilename_p)
            if not overwrite and aponame in groupapos.keys():
                return
            if coords is None:
                coords = self.split_coords_by_channels()
            groupapo = groupapos.create_group()
            groupapo.create_dataset('coords', data=coords)
            return

        else:
            raise ValueError(f'Unsupported key : {key}')
