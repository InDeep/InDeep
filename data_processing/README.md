#Data processing pipeline

The processing happens in three main steps : 
 - The integration of several file sources into a single directory
 - The construction of the hdf5 database from this directory
 - The loading of the hdf5 by a Pytorch Dataloader
 
## The integration of several file sources into a single directory
This can be skipped for users, since we will distribute this repository directly, and corresponds to the first lines of 
the build_database script

At first we need to dump the right pdb files. The database is filled with pdbs named like 2k5b-AB-P07900-Q16543-B.pdb
So basically a pdb code, the two chains interacting, uniprot codes and the one that is to be used as receptor.
We copy these into one folder for each system, organized in a hierarchical way for faster access.
Then we mine apo structures and dump them in the right folders. 
Then we go through the hetero dimers database and create '-short.pdb', ligands with only the residues closer than 6A.
Finally, we split those short partners into chunks, yielding 'short_{i}.pdb' files.

## The construction of the hdf5 database from a set of pdb files

Once we have the shorts in the database, we prepare an hdf5, by splitting the obtained pdb into channels 
using the Density.Coords_channels. This creates the default hdf5 and one can also create reduced versions of the hdf5
file filtered based on a text file. 
This filtering can also be made later on so that we only have to create the hdf5 once.
To create the hdf5, just run build_database.py

## The loading of the hdf5 file and use in learning
This step is involves two steps.
- The matrix format (n,4) is put into the grid format at the top of the Complex.py 
- The class Complex encapsulates the loading and the former transformations
into an interface that uses only the hdf5, a protein and a ligand
- Then the data is put into a Database object that is used by a Pytorch dataloader object
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 