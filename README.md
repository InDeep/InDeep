# InDeep : 3D convolutional neural networks to assist in silico drug design on protein-protein interactions 

## Description

This repository offers a toolbox for PPI drug discovery. 
It enables prediction of interactibility and ligandability sites 
over a protein structure. 
The scores associated with those values can also be monitored along a MD analysis.
These predictions can be visualised easily.

## Setup

### Getting the code
Simply clone the package by running : 
```
git clone https://gitlab.pasteur.fr/InDeep/InDeep.git
cd InDeep
```

### Packages

To run this code, one need to use python with several packages. 
The key packages to install are PyTorch (we recommend using the gpu version if one
is equipped with a gpu) and PyMol.
Even though users can install these packages manually, we recommend using a conda environment.
One should have conda installed and then run one of the following :

For a cpu-only installation

```
conda env create -f indeep_cpu.yml
```

For a GPU usage :
```
conda env create -f indeep_gpu.yml
```

And to activate this environment, run :
```
conda activate indeep
```

### PyMol plugin setup 

To setup the PyMol plugin, just activate your environment and type pymol in the 
commandline to launch PyMol.
Then click on the tab *Plugin* and *Plugin Manager* and on the window that pops, click on the tab *Settings*
In this tab, one can *Add new directory...* and select :
```ROOT_OF_PROJECT/InDeep_PyMol_plugin```

! Be careful : don't select the file InDeep_PyMol_plugin/InDeep' but stop at InDeep_PyMol_plugin/ to correctly see
the plugin !

Finally, one should quit PyMol and relaunch it by running pymol in the command line.
In the tab *Plugin*, there should be a new plugin : *InDeep* that one can use directly.
Enter a pymol selection and click the predict button. 
You should see the predictions and you can then use the pocket selector and probability sliders 
to see the most probable regions for binding.

On a CPU this can take a few seconds and PyMol might ask you whether you want to kill the program. 
Just click on wait, the predictions on this system will be saved so you only have to run the computation once.

## Command line usage 

After activating the conda environment, one can also directly run a prediction on
a PDB file or a MD trajectory in the DCD format.
```
python learning/predict.py pdb -p PATH_TO_PDB
python learning/predict.py traj -p PATH_TO_PDB -d PATH_TO_DCD
```

For an explanation of all options, run :
```
python learning/predict.py --help
```

## Example usage

We examplify our pipeline of the analysis on the usecase of the binding of MDM2 to P53.
This interaction is observed in the PDB entry 1ycr.

First, we open the structure in pymol. 
Then one can use the PyMol plugin to predict interactability.

![1ycr_static](example/1ycr_static.png)

We see that the predicted interactability patch is actually on the right spot.

We have performed a very short MD on this structure.
One can easily monitor the interactibility at this interaction site using the center of mass
of the predicted pocket as an anchor, by running : 

```
python3 learning/predict.py traj -p example/1ycr.pdb -t example/1ycr.dcd --pl \
-o example/out.txt -d example/out -c "chain A" --anchor 24.0 77.6 53.1
```

This will create a text file out.txt containing the scores for each frame of the trajectory.
It will also create a dcd file example/out.dcd for visualization of those results.
We provide an animation of this output in GIF format :

![1ycr_dynamic](example/1ycr_dynamic.gif)


## Reproducing the results

### Data
The data is the one of the IPPI-DB. A download link will be set up shortly, for now, 
please contact the authors to get the database.

Once equiped with the database, one can turn it into an hdf5 file by running 
```
python data_processing/build_hdf5.py
```

This file can then be used in the learning pipeline directly, along with text files to specify the data splits.
The text files we used were based on CATH classes splitting and are available in data/

### Learning
Read the README in learning to launch the training of a new model.
This new model should then appear in the PyMol plugin or be called explicitly 
using the command line.

### Benchmark
The scripts necessary for reproducing both the PL and HD plots and results are 
present in the benchmark/ folder.





